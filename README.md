### Начало работы

- Сделать fork репозитория;
- В боковой понели GitLab в пункте Manage выбрать Members и добавить группу...
  Это позволит позже дать знать менторам, что вы готовы сдать дз на проверкку;
- В той же панели выбрать Build -> PipeLine и выбрать .gitlab-ci.ylm для сборки вашего проекта, а именно
  проведенеия автоматических тестов (как модульных, так и функциональных)
- Перейти в ветку hw*, где вместо * -- номер интересующего дз
- Создать новую ветку, именно в ней вы будете делать ваше дз
- Когда вы решите, что готовы к сдаче, необъходимо создать merge request вашей
  рабочей векти **в ветку с текущим дз в вашего репозитория.**
- Ждать ревью) Если у менторов не будет к вам вопросов, то
  ваш merge request будет принят

### Ссылки на лекции

| № |                Название                |                                                                                                                                                            Материалы                                                                                                                                                             |                                          Запись лекции                                           |
|:-:|:--------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------:|
| 1 |               Знакомство               |                                                                                        [-> клик <-](https://docs.google.com/presentation/d/e/2PACX-1vQCFbVHsNIqzSLNZYGxz9p4DJzJOs4d6M5qBPWfrPq-bceEW371000pbFMjrNXlJ_h6mKb4iEuIe83_/pub)                                                                                         |                                  ![](.materials/увы-jokerge.gif)                                  |
| 2 |              Вброс в Git               |                                                                                        [-> клик <-](https://docs.google.com/presentation/d/e/2PACX-1vRX3CqkW35xWio21jZXlmyokTDFUqBfnCyKomREdO9ngu01UrYT0d_KmT2gIU7PUa0tWZpePTgjH9q2/pub)                                                                                         | [-> клик <-](https://drive.google.com/file/d/1aNrlFA_Zl8lwfpic5-s1D7qp9q8PiFKo/view?usp=sharing) |
| 3 |  Git. Работа с удаленным репозиторием  |                                                                                        [-> клик <-](https://docs.google.com/presentation/d/e/2PACX-1vSynGgcE5_XCc2EnMowTpKA0c5bHnhlt4m3AvrbAECXw6CkmhPkxGDCKdeKEwAi4wvuhy_tUeS-uusv/pub)                                                                                         |                                  ![](.materials/увы-jokerge.gif)                                  |
| 4 | Основы программирования на Go(часть 1) | [Основы программирования и алгоритмизации](https://docs.google.com/presentation/d/e/2PACX-1vTGk-nztia_TzGSf6-1SCGit9do5rF8LOr4xgVFt8-eDnz2jZ3uLr4C_Ux3DdTFn5TFPQxEW58j9U1z/pub) и [База Go](https://docs.google.com/presentation/d/e/2PACX-1vTkiPN1STAvJDgGT0fZ7aw1Nh9OKXk5hyE70xBDoyMqcdGI_ABcTNpPIsKv2GMHmH2cR1JVtzOl9U02/pub) | [-> клик <-](https://drive.google.com/file/d/14gHHjCA5EKg60iEcLjcHInFx92LWI2fp/view?usp=sharing) |
| 5 | Основы программирования на Go(часть 2) |                                                                                      [-> клик <-](https://github.com/golang-standards/project-layout/tree/master) и [-> клик <-](https://github.com/stud-it-department/lections/tree/lec-4)                                                                                      | [-> клик <-](https://drive.google.com/file/d/15ZpU0DTkvDVn9bJEOYG081BWGiaY2XRE/view?usp=sharing) |

### Домашние задания

1) [Знакомство с GoLang.](https://gitlab.com/test5030344/test/-/tree/hw1?ref_type=heads)
2) [link2]()